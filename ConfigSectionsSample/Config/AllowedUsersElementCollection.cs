﻿using System.Configuration;

namespace ConfigSectionsSample.Config
{
    public class AllowedUsersElementCollection : ConfigurationElementCollection
    {
        public override bool IsReadOnly()
        {
            return false;
        }

        public void Add(UserElement user)
        {
            BaseAdd(user);
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new UserElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((UserElement) element).Name;
        }
    }
}