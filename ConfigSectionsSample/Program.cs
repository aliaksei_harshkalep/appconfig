﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigSectionsSample.Config;

namespace ConfigSectionsSample
{
    class Program
    {
        static void Main(string[] args)
        {
            // For web apps System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            var cfg = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            var section = (GlobalSettingsSection) cfg.GetSection("globalSettingsGroup/globalSettings");

            // Read some properties
            Console.WriteLine(section.Url);
            Console.WriteLine(section.ConnectionOptions.SendTimeout);

            foreach (UserElement allowedUser in section.AllowedUsers)
            {
                Console.WriteLine(allowedUser.Name);
                Console.WriteLine(allowedUser.Password);
            }

            // read dictionary
            var custom = (NameValueCollection)ConfigurationManager.GetSection("globalSettingsGroup/customSettings");
            foreach (var c in custom.AllKeys)
            {
                Console.WriteLine(c);
                Console.WriteLine(custom[c]);
            }

            // Change something
            section.ConnectionOptions.SendTimeout = 10;
            // new user
            section.AllowedUsers.Add(new UserElement {Name = "Hello", Password = "World"});

            // save
            cfg.Save(ConfigurationSaveMode.Full);
        }
    }
}
