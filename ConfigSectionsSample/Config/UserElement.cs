﻿using System.Configuration;

namespace ConfigSectionsSample.Config
{
    public class UserElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return (string) this["name"]; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("password", IsRequired = true, DefaultValue = "11111")]
        [StringValidator(MinLength = 5)]
        public string Password
        {
            get { return (string) this["password"]; }
            set { this["password"] = value; }
        }
    }
}