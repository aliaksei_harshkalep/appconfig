﻿using System.Configuration;

namespace ConfigSectionsSample.Config
{

    public class ConnectionOptionsElement : ConfigurationElement
    {
        [ConfigurationProperty("sendTimeout", DefaultValue = 100, IsRequired = false)]
        [IntegerValidator(MinValue = 100)]
        public int SendTimeout
        {
            get { return (int) this["sendTimeout"]; }
            set { this["sendTimeout"] = value; }
        }

        [ConfigurationProperty("duration", DefaultValue = 0.5f, IsRequired = false)]
        public float Duration
        {
            get { return (float) this["duration"]; }
            set { this["duration"] = value; }
        }
    }
}