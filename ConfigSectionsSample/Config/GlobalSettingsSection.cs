﻿using System.Configuration;

namespace ConfigSectionsSample.Config
{
    public class GlobalSettingsSection : ConfigurationSection
    {
        [ConfigurationProperty("url", DefaultValue = "http://google.com", IsRequired = false)]
        public string Url
        {
            get { return (string) this["url"]; }
            set { this["url"] = value; }
        }

        [ConfigurationProperty("connectionOptions", IsRequired = true)]
        public ConnectionOptionsElement ConnectionOptions
        {
            get { return (ConnectionOptionsElement) this["connectionOptions"]; }
            set { this["connectionOptions"] = value; }
        }

        [ConfigurationProperty("allowedUsers")]
        [ConfigurationCollection(typeof(AllowedUsersElementCollection), AddItemName = "user", ClearItemsName = "clear", RemoveItemName = "remove")]
        public AllowedUsersElementCollection AllowedUsers
        {
            get { return (AllowedUsersElementCollection) this["allowedUsers"]; }
            set { this["allowedUsers"] = value; }
        }
    }
}